import random
import string
import json

from django.shortcuts import render
from django.views.generic import TemplateView


class HomePageView(TemplateView):

    def get(self, request, **kwargs):

        airports = ("Oslo", "London", "Munich",
                    "Helsinki", "Amsterdam", "Moscow", "Paris", "Berlin")

        flights = []

        for i in range(100):
            flights.append({
                "airport": random.choice(airports),
                "id": ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
            })

        context = {}
        context["flights"] = flights
        context["flights_json"] = json.dumps(flights)

        return render(request, 'index.html', context)
