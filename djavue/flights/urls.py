from django.conf.urls import url
from flights import views


urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
]
